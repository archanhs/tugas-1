//Soal 1
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
var kalimat = word.concat(" ", second, " ", third, " ",fourth, " ",fifth, " ",sixth, " ",seventh);
console.log(kalimat)
//Soal 2
var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence[5]+sentence[6]+sentence[7]+sentence[8]+sentence[9]; // lakukan sendiri 
var fourthWord = sentence[11]+sentence[12]; // lakukan sendiri 
var fifthWord = sentence[14]+sentence[15]; // lakukan sendiri 
var sixthWord = sentence[17]+sentence[18]+sentence[19]+sentence[20]+sentence[21]; // lakukan sendiri 
var seventhWord = sentence[23]+sentence[24]+sentence[25]+sentence[26]+sentence[27]+sentence[28]; // lakukan sendiri 
var eighthWord = sentence[30]+sentence[31]+sentence[32]+sentence[33]+sentence[34]+sentence[35]+sentence[36]+sentence[37]+sentence[38]; // lakukan sendiri 

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)
//Soal 3
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); // do your own! 
var thirdWord2 = sentence2.substring(15, 17); // do your own! 
var fourthWord2 = sentence2.substring(18, 20); // do your own! 
var fifthWord2 = sentence2.substring(21, 25); // do your own! 

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);
//Soal B 1
var nama = "Jenita"
var peran = "Guard"

if(nama == '' && peran == ''){
    console.log("Nama harus diisi!!")
}else if(nama == 'john' && peran == ''){
    console.log("Halo John, Pilih peranmu untuk memulai game!")
}else if(nama == 'Jane' && peran == 'Penyihir'){
    console.log("Selamat datang di Dunia Werewolf, Jane")
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
}else if(nama == 'Jenita' && peran == 'Guard'){
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
}else if(nama == 'Junaedi' && peran == 'Warewolf'){
    console.log("Selamat datang di Dunia Werewolf, Junaedi")
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
}
//Soal B 2
var hari = 21; 
var bulan = 13; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945';
switch(bulan){
    case 1: {bulan = "Januari";break;}
    case 2: {bulan = "Februari";break;}
    case 3: {bulan = "Maret";break;}
    case 4: {bulan = "April";break;}
    case 5: {bulan = "Mei";break;}
    case 6: {bulan = "Juni";break;}
    case 7: {bulan = "Juli";break;}
    case 8: {bulan = "Agustus";break;}
    case 9: {bulan = "September";break;}
    case 10: {bulan = "Oktober";break;}
    case 11: {bulan = "November";break;}
    case 12: {bulan = "Desember";break;}
    default: {bulan = "input tidak valid"}
}
console.log(hari+" "+bulan+" "+tahun)

